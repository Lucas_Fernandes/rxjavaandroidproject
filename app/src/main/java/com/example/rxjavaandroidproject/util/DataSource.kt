package com.example.rxjavaandroidproject.util

import com.example.rxjavaandroidproject.model.Task

object DataSource {

  fun createTaskList(): MutableList<Task> {
      return mutableListOf(
          Task("Take out the thrash", true, 3),
          Task("Walk the dog", false, 2),
          Task("Make my bed", true, 1),
          Task("Unload the dishwasher", false, 0),
          Task("Make Dinner", true, 5)
      )
  }

}