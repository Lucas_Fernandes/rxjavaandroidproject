package com.example.rxjavaandroidproject

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.rxjavaandroidproject.model.Task
import com.example.rxjavaandroidproject.util.DataSource
import io.reactivex.Observable
import io.reactivex.ObservableOnSubscribe
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

private const val TAG = "TAG"
private const val TAG2 = "TAG2"
private const val TAG3 = "TAG3"
private const val TAG4 = "TAG4"
private const val TAG5 = "TAG5"
private val compositeDisposable = CompositeDisposable()

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        createObservableFromIterable()
        createObservableFromScratch()
        createJustObservable()
        createRangeObservable()

        Observable.fromIterable(DataSource.createTaskList())
            .subscribeOn(Schedulers.io())
            .buffer(2)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object: Observer<List<Task>>{
                override fun onComplete() {
                }

                override fun onSubscribe(d: Disposable) {
                }

                override fun onNext(tasks: List<Task>) {
                    Log.d(TAG5, "onNext: -------------- bundle results ------------")
                    tasks.forEach { task ->
                        Log.d(TAG5, "onNext: ${task.description}")
                    }
                }

                override fun onError(e: Throwable) {
                }

            })

    }

    private fun createRangeObservable() {
        Observable.range(0, 9)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<Int> {

                override fun onComplete() {}

                override fun onSubscribe(d: Disposable) {}

                override fun onNext(integers: Int) {
                    Log.d(TAG4, "onNext: $integers")
                }

                override fun onError(e: Throwable) {}

            })
    }

    private fun createJustObservable() {
        Observable.just(Task("JUST Walk the dog", false, 3))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<Task> {

                override fun onComplete() {
                    Log.d(TAG3, "onComplete: just observer call completed")
                }

                override fun onSubscribe(d: Disposable) {}

                override fun onNext(task: Task) {
                    Log.d(TAG3, task.description)
                }

                override fun onError(e: Throwable) {
                    Log.e(TAG3, "onError: ", e)
                }
            })
    }

    private fun createObservableFromScratch() {
        val taskList = DataSource.createTaskList()
        Observable.create(ObservableOnSubscribe<Task> { emitter ->

            taskList.forEach { task ->
                if (!emitter.isDisposed) { //
                    emitter.onNext(task)
                }
            }

            if (!emitter.isDisposed) {
                emitter.onComplete()
            }

        }).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<Task> {
                override fun onComplete() {
                }

                override fun onSubscribe(d: Disposable) {
                }

                override fun onNext(task: Task) {
                    Log.d(TAG2, "onNext: ${task.description}")
                }

                override fun onError(e: Throwable) {
                }

            })
    }

    private fun createObservableFromIterable() {

        Observable.fromIterable(DataSource.createTaskList())
            .subscribeOn(Schedulers.io())
            .filter { t: Task -> // Doing the work on a back ground thread
                Log.d(TAG, "text: " + Thread.currentThread().name)
                t.isComplete
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<Task> { //doing the work on the main thread
                override fun onSubscribe(d: Disposable) {
                    Log.d(TAG, "onSubscribe called.")
                    compositeDisposable.add(d)
                }

                override fun onNext(task: Task) {
                    Log.d(TAG, "onNext: " + Thread.currentThread().name)
                    Log.d(TAG, "onNext: " + task.description)
                }

                override fun onError(e: Throwable) {
                    Log.e(TAG, "onError", e)
                }

                override fun onComplete() {
                    Log.d(TAG, "onComplete called.")
                }
            })
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }

}
